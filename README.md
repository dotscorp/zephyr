This is to build hci_uart application to be run on Clare's Nordic IC:  
$ cd ..../zephyr
$ source zephyr-env.sh
$ cd ..../zephyr/samples/bluetooth/hci_uart  
$ mkdir build  
$ cd build  
$ cmake -DBOARD=dots_clare ..  
$ make  

To load the app onto Clare's Nordic IC, connect JLink then issue these commands:  
$ nrfjprog --eraseall -f nrf52  
$ nrfjprog --program zephyr/zephyr.hex -f nrf52  
$ nrfjprog --reset -f nrf52  
